﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Media;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace keno
{
    public partial class home : Form

    {
        public static home instance;
        DataTable table = new DataTable("table");
        int index;
        string x;

        public home()
        {
            InitializeComponent();
            instance = this;
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (comboBox1.Text == "")
            {
                MessageBox.Show("Playing number can not be empty", "please Fill Playing Number", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                table.Rows.Add(Guid.NewGuid().ToString("N"), textBox1.Text, comboBox1.Text);
                textBox1.Text = string.Empty;
                comboBox1.Text = string.Empty;
            }
        

        }

        private void home_Load(object sender, EventArgs e)
        {
            int w = Screen.PrimaryScreen.Bounds.Width;
            int h = Screen.PrimaryScreen.Bounds.Height;
            this.Location = new Point(0, 0);
            this.Size = new Size(w, h);

            table.Columns.Add("ID", Type.GetType("System.String"));
            table.Columns.Add("Name", Type.GetType("System.String"));
            table.Columns.Add("Playing_Number", Type.GetType("System.String"));
            dataGridView1.DataSource = table;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }


            private void button2_Click(object sender, EventArgs e)
         {
           

            playingScreen ps = new playingScreen();
            ps.Show();
            this.Hide();

            axWindowsMediaPlayer1.URL = "C:\\Users\\HTU\\Downloads\\KENO VIDEO.MP4";
            axWindowsMediaPlayer1.Ctlcontrols.play();
            winnerTextfiled.Visible = true;

            Random rand = new Random();
                int counter = dataGridView1.Rows.Count;
                int rowIndex = rand.Next(counter - 1);
                int columnIndex = 2; // Replace with the index of the column you want to get the random value from
                object randomValue = dataGridView1.Rows[rowIndex].Cells[columnIndex].Value;
                object x = randomValue;
               Task.Delay(4000).Wait();
            playingScreen.instance.tb1.Text = x + "";

            SoundPlayer simpleSound = new SoundPlayer(@"c:\Windows\Media\chimes.wav");
            simpleSound.Play();




        }
     


        private void dispayResult()
        {
          
        }

        private void axWindowsMediaPlayer1_Enter(object sender, EventArgs e)
        {

        }
       
       
        private void btnStop_Click(object sender, EventArgs e)
        {
            axWindowsMediaPlayer1.Ctlcontrols.stop();
        }

        private void Fill(object sender, DataGridViewAutoSizeColumnsModeEventArgs e)
        {

        }

        private void button5_Click(object sender, EventArgs e)
        {
            registration reg = new registration();
            reg.Show();
            this.Hide();
        }

        private void dataGridView1_click(object sender, DataGridViewCellEventArgs e)
        {
            index = e.RowIndex;
            DataGridViewRow row = dataGridView1.Rows[index];
         
            textBox1.Text = row.Cells[1].Value.ToString();
            comboBox1.Text = row.Cells[2].Value.ToString();
              
        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            DataGridViewRow newdata = dataGridView1.Rows[index];
            newdata.Cells[1].Value = textBox1.Text;
            newdata.Cells[2].Value = comboBox1.Text;
            textBox1.Text = string.Empty;
            comboBox1.Text = string.Empty;

           
        }

        private void button4_Click(object sender, EventArgs e)
        {
            index = dataGridView1.CurrentCell.RowIndex;
            dataGridView1.Rows.RemoveAt(index);

        }


        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void axWindowsMediaPlayer2_Enter(object sender, EventArgs e)
        {
          
        }

        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {

            int selectedValue = Convert.ToInt32(comboBox2.SelectedItem);
            int recordCount = dataGridView1.Rows.Count;
            int netCount = recordCount - 2;
            textBox2.Text =  (selectedValue * netCount).ToString();
        }


    }


}

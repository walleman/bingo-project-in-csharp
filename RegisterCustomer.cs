﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace keno
{
    public partial class RegisterCustomer : Form
    {
        String connectionString;
        SqlConnection con;
        SqlDataAdapter cmd = new SqlDataAdapter();
        public RegisterCustomer()
        {
            InitializeComponent();
            connectionString = @"Data Source=DESKTOP-F6FEVE5;Initial Catalog=kenoDBOne;Integrated Security=False;Trusted_Connection=true;TrustServerCertificate=True;Encrypt=False;Connection Timeout=30;";
            con = new SqlConnection(connectionString);
            con.Open();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

       

        private void btnReg_Click(object sender, EventArgs e)
        {
          //save values  into customers table
            SqlCommand cmd = new SqlCommand("Insert into customers values(@id,@name,@phone,@playing_number)", con);
            cmd.Parameters.AddWithValue("@id", Guid.NewGuid().ToString("N"));
            cmd.Parameters.AddWithValue("@name", cname.Text);
            cmd.Parameters.AddWithValue("@phone", cphone.Text);
            cmd.Parameters.AddWithValue("@playing_number", playingNomber.Text);
            cmd.ExecuteNonQuery();

            //save values into daily players table
            SqlCommand cmdd = new SqlCommand("Insert into daily_players values(@id,@name,@phone,@playing_number)", con);
            cmdd.Parameters.AddWithValue("@id", Guid.NewGuid().ToString("N"));
            cmdd.Parameters.AddWithValue("@name", cname.Text);
            cmdd.Parameters.AddWithValue("@phone", cphone.Text);
            cmdd.Parameters.AddWithValue("@playing_number", playingNomber.Text);
            cmdd.ExecuteNonQuery();


            con.Close();
            home homepage = new home();
            homepage.Show();
            this.Hide();
            MessageBox.Show("sucessfully Register !");
        }

        private void email_TextChanged(object sender, EventArgs e)
        {

        }

        private void clear_daily_players_Click(object sender, EventArgs e)
        {

        }
    }
}

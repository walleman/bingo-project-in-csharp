﻿namespace keno
{
    partial class RegisterCustomer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnReg = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.cname = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.playingNomber = new System.Windows.Forms.ComboBox();
            this.clear_daily_players = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.cphone = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // btnReg
            // 
            this.btnReg.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.btnReg.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnReg.Location = new System.Drawing.Point(152, 254);
            this.btnReg.Name = "btnReg";
            this.btnReg.Size = new System.Drawing.Size(243, 33);
            this.btnReg.TabIndex = 26;
            this.btnReg.Text = "Register";
            this.btnReg.UseVisualStyleBackColor = false;
            this.btnReg.Click += new System.EventHandler(this.btnReg_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(34, 162);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(112, 18);
            this.label3.TabIndex = 20;
            this.label3.Text = "Playing Number";
            // 
            // cname
            // 
            this.cname.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cname.Location = new System.Drawing.Point(88, 41);
            this.cname.Multiline = true;
            this.cname.Name = "cname";
            this.cname.Size = new System.Drawing.Size(214, 28);
            this.cname.TabIndex = 17;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(34, 41);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(48, 18);
            this.label1.TabIndex = 16;
            this.label1.Text = "Name";
            // 
            // playingNomber
            // 
            this.playingNomber.FormattingEnabled = true;
            this.playingNomber.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10",
            "11",
            "12",
            "13",
            "14",
            "15",
            "16",
            "17",
            "18",
            "19",
            "20",
            "21",
            "22",
            "23",
            "24",
            "25",
            "26",
            "27",
            "28",
            "29",
            "30",
            "31",
            "32",
            "33",
            "34",
            "35",
            "36",
            "37",
            "38",
            "39",
            "40",
            "41",
            "42",
            "43",
            "44",
            "45",
            "46",
            "47",
            "48",
            "49",
            "50"});
            this.playingNomber.Location = new System.Drawing.Point(169, 163);
            this.playingNomber.Name = "playingNomber";
            this.playingNomber.Size = new System.Drawing.Size(185, 21);
            this.playingNomber.TabIndex = 27;
            this.playingNomber.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // clear_daily_players
            // 
            this.clear_daily_players.BackColor = System.Drawing.Color.IndianRed;
            this.clear_daily_players.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.clear_daily_players.Location = new System.Drawing.Point(607, 17);
            this.clear_daily_players.Name = "clear_daily_players";
            this.clear_daily_players.Size = new System.Drawing.Size(179, 32);
            this.clear_daily_players.TabIndex = 28;
            this.clear_daily_players.Text = "Clear Daily Players";
            this.clear_daily_players.UseVisualStyleBackColor = false;
            this.clear_daily_players.Click += new System.EventHandler(this.clear_daily_players_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(34, 98);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(51, 18);
            this.label2.TabIndex = 18;
            this.label2.Text = "Phone";
            // 
            // cphone
            // 
            this.cphone.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cphone.Location = new System.Drawing.Point(88, 98);
            this.cphone.Multiline = true;
            this.cphone.Name = "cphone";
            this.cphone.Size = new System.Drawing.Size(214, 28);
            this.cphone.TabIndex = 19;
            this.cphone.TextChanged += new System.EventHandler(this.email_TextChanged);
            // 
            // RegisterCustomer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.clear_daily_players);
            this.Controls.Add(this.playingNomber);
            this.Controls.Add(this.btnReg);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.cphone);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.cname);
            this.Controls.Add(this.label1);
            this.Name = "RegisterCustomer";
            this.Text = "RegisterCustomer";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button btnReg;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox cname;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox playingNomber;
        private System.Windows.Forms.Button clear_daily_players;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox cphone;
    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;

namespace keno
{
    public partial class registration : Form
    {
        String connectionString;
        SqlConnection con;
        SqlDataAdapter cmd = new SqlDataAdapter();

        public registration()
        {
            InitializeComponent();
            connectionString = @"Data Source=DESKTOP-F6FEVE5;Initial Catalog=kenoDBOne;Integrated Security=False;Trusted_Connection=true;TrustServerCertificate=True;Encrypt=False;Connection Timeout=30;";
            con = new SqlConnection(connectionString);
            con.Open();
        }
    

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {
            
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Form1 login = new Form1();
            login.Show();
            this.Hide();
        }
      
        private void btnReg_Click(object sender, EventArgs e)
        {
            if (name.Text == "")
            {
                    MessageBox.Show("please enter valid name", "error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                
            }
            else if (email.Text == "")
            {
                MessageBox.Show("please enter valid email", "error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (phone.Text == "")
            {
                MessageBox.Show("please enter valid Phone", "error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            else if (pass.Text == "")
            {
                MessageBox.Show("please enter valid pasword", "error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (pass.Text != confirmPass.Text)
            {
                MessageBox.Show("password does not mach", "error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {

                SqlCommand cmd = new SqlCommand("Insert into users values(@id,@name,@email,@phone,@password)", con);
                cmd.Parameters.AddWithValue("@id", Guid.NewGuid().ToString("N"));
                cmd.Parameters.AddWithValue("@name", name.Text);
                cmd.Parameters.AddWithValue("@email", email.Text);
                cmd.Parameters.AddWithValue("@phone", phone.Text);
                cmd.Parameters.AddWithValue("@password", pass.Text);
                cmd.ExecuteNonQuery();


                con.Close();
                home homepage = new home();
                homepage.Show();
                this.Hide();
                MessageBox.Show("sucessfully Register !");
            }
            

        }

        private void button1_Click(object sender, EventArgs e)
        {
            home h = new home();
            h.Show();
            this.Hide();
        }
    }

}
